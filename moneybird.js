(function ($) {

setTimeout(
  function() 
  {
	$.fn.colorbox({
		href: Drupal.settings.moneybird.moneybirdInvoiceUrl,
		iframe: true,
		width: 850,
		height: '95%',
	});
  }, 100);

})(jQuery);
